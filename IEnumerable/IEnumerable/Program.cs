﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = new TreesValue(21)
            {
                156,
                65,
                45,
                5641,
                545,
                4541,
                551561,
                4545,
                5451,
                56156,
            };

            int max = test.value;

            foreach (int item in test)
            {
                if (max < item)
                {
                    max = item;
                }
                Console.WriteLine("test number- {0} ", item);
            }

            Console.WriteLine("Max = {0}", max);
        }
    }

    class TreesValue : IEnumerable
    {
        public int value;
        public TreesValue next;
        public TreesValue second;

        public TreesValue(int item)
        {
            value = item;
        }

        public void Add(int item)
        {
            if (second == null)
            {
                next = new TreesValue(item);
                second = next;
            }
            else
            {
                second.next = new TreesValue(item);
                second = second.next;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        public override string ToString()
        {
            return value.ToString();
        }

        class Enumerator : IEnumerator
        {
            public TreesValue root;
            public object Current { get; set; }

            public Enumerator(TreesValue node)
            {
                root = node;
            }

            public bool MoveNext()
            {
                if (root == null)
                    return false;
                Current = root.value;
                root = root.next;
                return true;
            }

            public void Reset()
            {
                root = null;
            }
        }
    }
}
